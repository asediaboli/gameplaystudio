# Escape Game ONLINE

## Environnement technique

- [JavaSE 8](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Maven 3.6](https://maven.apache.org/download.cgi)
- Dépendances   
  Log4J 2.12  
  Junit 4.12


#### Instructions d'installation et de configuration
- [Java](https://docs.oracle.com/en/java/javase/13/install/overview-jdk-installation.html)
- [Maven](https://maven.apache.org/install.html)

## Lancement
### Première méthode
+ Cloner le dépot de GitLab

+ Ouvrir le projet dans IDE (Eclipse, Intellij IDEA)
+ Exécuter la classe Start.java

### Deuxième méthode
- Télécharger le dépot de GitLab

- Décompresser dans le dossier souhaité

- Entrer le dossier **gameplaystudio-master**. Où vous verrez le dossier
  **src**.
- Lancer la console.  
**WINDOWS:** Dans le dossier gameplaystudio-master, resserrer la touche
Shift et faites un clic droit et sélectionnez ***Ouvrir la fenêtre
PowerShell ici.***  
**LINUX:** Lancer Terminal et entrer **cd
"path/to/gameplaystudio-master"**
- Compiler le projet.  
*mvn compile*
- Lancer le projet.  
*mvn exec:java -Dexec.mainClass="Start"*  
## Utilisation du fichier de configuration du jeu

Le dossier contient le fichier **config.properties**. Dans lequel vous pouvez modifier trois paramètres.

    modeDev                        0 - mode développeur désactivé, 1 - mode développeur activé. Valeurs acceptées sont 0 et 1.
    numberOfNumbersInGameSet       nombre de chiffres en combinaison. Valeurs acceptées sont de 1 à 20.
    numberOfAttempt                nombre d'essais. Valeurs acceptées sont de 1 à 10.