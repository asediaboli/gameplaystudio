package config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/*
    Lecture du fichier de paramètres et création du singleton de ConfigGame
 */
public class ConfigGame {
    private static final Logger LOG = LogManager.getLogger(ConfigGame.class.getName());

  private int modeDev;
    private int numberOfNumbersInGameSet;
    private int numberOfAttempts;

    public int getModeDev() {
        return modeDev;
    }

    public int getNumberOfNumbersInGameSet() {
        return numberOfNumbersInGameSet;
    }

    public int getNumberOfAttempts() {
        return numberOfAttempts;
    }

    private static ConfigGame instance;

    private ConfigGame() {}

    public static ConfigGame getInstance() {
        if (instance == null) {
            instance = new ConfigGame();
            try {
                FileInputStream file = new FileInputStream("config.properties");
                Properties properties = new Properties();
                properties.load(file);

                instance.modeDev = checkModeDev(isInt(properties.getProperty("modeDev")));
                instance.numberOfNumbersInGameSet = checkNumberOfNumbersInGameSet(isInt(properties.getProperty("numberOfNumbersInGameSet")));
                instance.numberOfAttempts = checkNumberOfAttempts(isInt(properties.getProperty("numberOfAttempts")));
            } catch (IOException er) {
                LOG.error("File config.properties Missing");
            }
        }
        return instance;
    }

    /*
        Verification de données reçues, si ce n'est pas un nombre, nous affichons une erreur
     */
    private static int isInt(String propertiesValue) {
        try {
            return Integer.parseInt(propertiesValue);
        } catch (NumberFormatException er) {
            LOG.error(er.getMessage() + "\n Erreur de données dans le fichier config.properties. " +
                    "Assurez-vous que vous entrez un INTEGER dans chaque ligne");
            System.exit(0);
            return Integer.MIN_VALUE;
        }
    }

    /*
        Vérification de la plage valide
     */
    private static int checkModeDev(int value) {
        if (value < 0 || value > 1) {
            LOG.error("Valeur impossible dans le fichier config.properties, ligne - modeDev. " +
                    "Les valeurs acceptées sont 0 et 1");
            System.exit(0);
        }
        return value;
    }

    private static int checkNumberOfNumbersInGameSet(int value) {
        if (value <= 0 || value > 20) {
            LOG.error("Valeur impossible dans le fichier config.properties, ligne - NUMBERS_IN_GAME_SET. " +
                    "Les valeurs acceptées sont de 1 à 20");
            System.exit(0);
        }
        return value;
    }

    private static int checkNumberOfAttempts(int value) {
        if (value <= 0 || value > 10) {
            LOG.error("Valeur impossible dans le fichier config.properties, ligne - NUMBER_OF_ATTEMPTS. " +
                    "Les valeurs acceptées sont de 1 à 10");
            System.exit(0);
        }
        return value;
    }
}
