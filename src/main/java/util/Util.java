package util;

import java.util.Scanner;

public class Util {

    public static String arrayToString(int[] arrayOfNumbers) {  ///
        StringBuilder sb = new StringBuilder();
        for (int i : arrayOfNumbers) {
            sb.append(i);
        }
        return sb.toString();
    }

    public static void pressEnterToContinue() {
        Scanner s = new Scanner(System.in);
        s.nextLine();
    }
}
