package menu;

import mode.ModeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Menu {

    static final Logger LOG = LogManager.getLogger(Menu.class.getName());

    private static final int DUEL = 0;
    private static final int DEFENDER = 1;
    private static final int CHALLENGER = 2;
    private static final int EXIT_FIRST_MENU = 3;
    private static final int REPLAY = 0;
    private static final int ANOTHER_MODE = 1;
    private static final int EXIT_SECOND_MENU = 2;

    private String titleMenu;
    private String currentMode;
    private ModeFactory modeFactory;

    /*
        Constructeurs

        Menu()   pour commencer le jeu

        Menu(String currentMode)    pour redémarrer le jeu
     */
    public Menu() {
        this.titleMenu = "Escape Game ONLINE \n" +
                "Pour démarrer le jeu, sélectionnez le mode en entrant un numéro dans la console\n" +
                "0 - Duel \n" +
                "1 - Défenseur \n" +
            "2 - Challenger \n" +
                "3 - Quitter";
        this.currentMode = null;
        this.modeFactory = new ModeFactory();
    }

    public Menu(String currentMode) {
        this.titleMenu = "Voulez-vous rejouer ? (0 : rejouer au même mode, 1 : jouer à un autre mode, 2 : quitter le jeu)";
        this.currentMode = currentMode;
        this.modeFactory = new ModeFactory();
    }

    /*
        Méthode principale
     */
    public void startMenu() {
        System.out.println(titleMenu);
        int choice = getChoice();
        runSelectedMenu(choice);

    }

    private void runSelectedMenu(int choice) {
        if (currentMode == null) {
            switch (choice) {
                case DUEL:
                    LOG.info("Joueur a lancé le mode Duel");
                    modeFactory.getModeInterface("DUEL").start();
                    break;
                case DEFENDER:
                    LOG.info("Joueur a lancé le mode Defender");
                    modeFactory.getModeInterface("DEFENDER").start();
                    break;
                case CHALLENGER:
                    LOG.info("Joueur a lancé le mode Challenger");
                    modeFactory.getModeInterface("CHALLENGER").start();
                    break;
                case EXIT_FIRST_MENU:
                    LOG.info("Joueur a terminé l'application.");
                    System.exit(0);
                    break;
            }
        } else {
            switch (choice) {
                case REPLAY:
                    LOG.info("Joueur a lancé le meme mode");
                    modeFactory.getModeInterface(currentMode).start();
                case ANOTHER_MODE:
                    LOG.info("Joueur sélectionnera un autre mode.");
                    Menu menu2 = new Menu();
                    menu2.startMenu();
                case EXIT_SECOND_MENU:
                    LOG.info("Joueur a terminé l'application.");
                    System.exit(0);
            }
        }

    }

    /*
        Obtenir la sélection du joueur
     */
    private int getChoice() {
        Scanner scan = new Scanner(System.in);
        int number;
        if (currentMode == null) {
            do {
                System.out.println("Veuillez entrer un numéro (0 - 3)");
                number = scan.nextInt();
            } while (number < 0 || number > 3);
        } else {
            do {
                System.out.println("Veuillez entrer un numéro (0 - 2)");
                number = scan.nextInt();
            } while (number < 0 || number > 2);
        }
        return number;
    }
}
