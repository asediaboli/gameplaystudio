package io;

import java.util.Random;

public abstract class RandomNumbers {

    /*
        La première proposition de l'intelligence artificielle composée de 5
     */
    private static int[] takeFirstSetAI(int quantityNumbers) {
        int[] temp = new int[quantityNumbers];
        int i = 0;
        while (i < quantityNumbers) {
            temp[i] = 5;
            i++;
        }
        return temp;
    }

    /*
        La proposition de l'intelligence artificielle composée en Random
     */
    public static int[] takeFollowingSetAI(int quantityNumbers) {
        int[] set = new int[quantityNumbers];
        for (int i = 0; i < set.length - 1; i++) {
            Random random = new Random();
            set[i] = random.nextInt(10);
        }
        return set;
    }

    /*
        Comparer deux tableaux et recevoir une réponse basée sur eux
     */
    public static String takeArrayComparisonResult(int[] attack, int[] defense) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i <= attack.length - 1; i++) {
            if (attack[i] < defense[i]) {
                result.append("+");
            } else if (attack[i] > defense[i]) {
                result.append("-");
            } else {
                result.append("=");
            }
        }
        return result.toString();
    }

    /*
        Rechercher un gagnant.
        Comparer une réponse à une base
     */
    public static boolean isWin(String arrayComparisonResult, int quantityNumbers) {        // True si arrayComparisonResult = "===="
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < quantityNumbers; i++) {
            sb.append("=");
        }
        return sb.toString().equals(arrayComparisonResult);
    }

    /*
        Propositions ultérieures basées sur une réponse du joueur
     */
    public static int[] takeResponseAI(int[] previousArray, String responsePlayer) {
        char[] temp = responsePlayer.trim().toCharArray();
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == '-') {
                previousArray[i]--;
            } else if (temp[i] == '+') {
                previousArray[i]++;
            }
        }
        return previousArray;
    }

    /*
        Le choix de la méthode en fonction de la ronde du jeu
     */
    public static int[] takeSuggestedByAI(int gameTourNumber, int[] numberSuggestedByAI, int numberOfNumbersInGameSet, String responsePlayer) {
        if (gameTourNumber == 1) {
            return takeFirstSetAI(numberOfNumbersInGameSet);
        } else {
            return takeResponseAI(numberSuggestedByAI, responsePlayer);   //Propositions suivantes basées sur la réponse du joueur
        }
    }
}
