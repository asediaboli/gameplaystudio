package io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public abstract class IOClass {

    private static final Logger LOG = LogManager.getLogger(IOClass.class.getName());

    /*
        Convertir une chaîne en un tableau de nombres
     */
    public static int[] inputUserSetNumbers(int quantityNumbers) {
        int whileA = 0;
        String temp = "";
        int[] numbersUser = new int[quantityNumbers];

        while (whileA == 0) {
            temp = getIntFromScanner(quantityNumbers);
            if (!temp.equals("0")) {
                whileA++;
            }
        }
        char[] setNumbTemp = temp.toCharArray();
        for (int i = 0; i < setNumbTemp.length; i++) {
            numbersUser[i] = Character.getNumericValue(setNumbTemp[i]);
        }
        return numbersUser;
    }

    /*
        Recevoir la réponse entrée par l'utilisateur et sa validation
    */
    public static String inputPlayerResponse(int quantityNumbers) {
        int b = 0;
        String resp = "";
        while (b == 0) {
            Scanner scanner = new Scanner(System.in);
            String st = scanner.nextLine().trim();
            if (st.length() == quantityNumbers & st.matches("[-=+]*")) {
                b++;
                resp = st;
            } else {
                LOG.info("Données mal saisies");
                System.out.println("Données mal saisies");
            }
        }
        return resp;
    }

    /*
        Obtenir le numéro saisi par l'utilisateur et sa validation
     */
    private static String getIntFromScanner(int quantityNumbers) {
        Scanner scan = new Scanner(System.in);
        String intsInput = scan.nextLine();
        if (intsInput.length() == quantityNumbers && intsInput.matches("[0-9]*")) {
            return intsInput;
        } else {
            String s = "Le nombre proposé ne convient pas \n" +
                    "Entrez un nombre positif à " + quantityNumbers + " chiffres";
            LOG.info(s);
            System.out.println(s);
            return "0";
        }
    }
}
