package mode;

public interface ModeInterface {

    void start();

    int startTour(int gameTourNumber);

    void printRulesOfGame(String[] rulesOfGame);

    void printTour(int gameTourNumber);

    void outputWinner(int userWin, int iaWin);

    void checkStandoff(int gameTourNumber, String currentMod);
}
