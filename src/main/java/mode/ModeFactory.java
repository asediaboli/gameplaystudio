package mode;

public class ModeFactory {

    public ModeInterface getModeInterface(String modeType) {
        if (modeType == null) {
            return null;
        }
        if (modeType.equalsIgnoreCase("DUEL")) {
            return new Duel();
        } else if (modeType.equalsIgnoreCase("DEFENDER")) {
            return new Defender();
        } else if (modeType.equalsIgnoreCase("CHALLENGER")) {
            return new Challenger();
        }
        return null;
    }
}
