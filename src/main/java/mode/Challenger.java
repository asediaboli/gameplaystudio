package mode;

import io.IOClass;
import io.RandomNumbers;
import util.Util;

public class Challenger extends Mode {

    private int[] numberSuggestedByPlayer;
    private String arrayComparisonResult;

    //Description du mode et des règles du jeu
    private final String[] rulesOfGame = {
            "****Bienvenue en mode CHALLENGER**** \n ***********Règles du jeu***********",

        "1. Ordinateur joue le rôle de défenseur. Elle définit une combinaison de " + Mode.NUMBERS_IN_GAME_SET + " chiffres aléatoirement\n" +
            "2. Joueur a le rôle d’attaquant et doit faire une proposition d’une combinaison de " + Mode.NUMBERS_IN_GAME_SET + " chiffres\n" +
                    "3. Ordinateur indique pour chaque chiffre de la combinaison proposée si le chiffre de sa combinaison est plus grand (+), plus petit (-) ou si c’est le bon (=)\n" +
            "4. Nombre d’essais sont " + Mode.NUMBER_OF_ATTEMPTS + "\n",

        "Entrez " + Mode.NUMBERS_IN_GAME_SET + " chiffres"};

    /*
        Constructeurs
     */
    Challenger() {
        secretCombinationOfAI = RandomNumbers.takeFollowingSetAI(Mode.NUMBERS_IN_GAME_SET);              //La combinaison secrète de l'intelligence artificielle
        this.gameTourNumber = 1;
    }

    @Override
    public void start() {
        super.start(rulesOfGame);
    }

    /*
        Méthode de base. Résultat = présence du vainqueur dans ce Tour de jeu
     */
    @Override
    public int startTour(int gameTourNumber) {
        if (gameTourNumber == 1) {
            checkDevMod(secretCombinationOfAI, gameTourNumber);     //Si le mode développeur est activé, affiche une combinaison secrète
        }
        numberSuggestedByPlayer = IOClass.inputUserSetNumbers(Mode.NUMBERS_IN_GAME_SET);    //Proposition du joueur
        arrayComparisonResult = RandomNumbers.takeArrayComparisonResult(numberSuggestedByPlayer, secretCombinationOfAI);    //Comparaison de la combinaison du joueur avec la combinaison d’IA et le résultat (+-+-)
        if (RandomNumbers.isWin(arrayComparisonResult, Mode.NUMBERS_IN_GAME_SET)) {
            return 1;
        } else {
            outputProposAndRespon(numberSuggestedByPlayer, arrayComparisonResult);
            return 0;
        }
    }

    /*
        Sortie sur la console des propositions du joueur et de la réponse de l’intelligence artificielle
     */
    private void outputProposAndRespon(int[] arrayToDisplay, String arrayComparisonResult) {
        String s = "Proposition du Joueur : "
                + Util.arrayToString(arrayToDisplay)
                + " -> Réponse d'IA : "
                + arrayComparisonResult;
        super.outputProposition(s);

    }

    @Override
    void outputWinner(int userWin) {
        super.outputWinner(userWin, 0);

    }
}
