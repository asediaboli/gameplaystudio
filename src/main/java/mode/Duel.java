package mode;

import menu.Menu;

import static mode.Mode.NUMBERS_IN_GAME_SET;
import static mode.Mode.NUMBER_OF_ATTEMPTS;

public class Duel implements ModeInterface {

    //Description du mode et des règles du jeu
    private final String[] rulesOfGame = {"****Bienvenue en mode Duel**** \n*********Règles du jeu*********",

        "1. Joueur et l’intelligence artificielle de l’ordinateur jouent Tour à Tour. Le premier à trouver la combinaison secrète de l'autre a gagné!\n"
                + "2. Combinaison de " + NUMBERS_IN_GAME_SET + " chiffres\n"
                + "3. Nombre d’essais sont " + NUMBER_OF_ATTEMPTS,

        "Imaginez " + NUMBERS_IN_GAME_SET + " chiffres et appuyez sur Enter"};
    private ModeInterface defender;
    private ModeInterface challenger;
    private int gameTourNumber;
    private String currentMode;
    private Menu menu;

    /*
        Constructeurs
     */
    Duel() {
        ModeFactory factories = new ModeFactory();
        this.defender = factories.getModeInterface("DEFENDER");
        this.challenger = factories.getModeInterface("CHALLENGER");
        this.currentMode = getClass().getSimpleName();
        this.menu = new Menu(currentMode);
        this.gameTourNumber = 1;
    }

    @Override
    public void start() {
        int winner;
        printRulesOfGame(rulesOfGame); //Appeler le menu des règles
        while (gameTourNumber <= NUMBER_OF_ATTEMPTS) {
            printTour(gameTourNumber); //Numéro du printTour de jeu

            winner = startTour(gameTourNumber);
            if (winner == 1) {
                menu.startMenu();
                break;
            }
            checkStandoff(gameTourNumber, currentMode);
            gameTourNumber++;
        }
    }

    @Override
    public int startTour(int gameTourNumber) {
        int iaWin = defender.startTour(gameTourNumber); //Obtenir le gagnant après avoir terminé la partie Defender       1 = Gagnant, 0 = Non

        System.out.println("Votre tour de proposer **" + NUMBERS_IN_GAME_SET + "** chiffres");

        int userWin = challenger.startTour(gameTourNumber); //Obtenir le gagnant après avoir terminé la partie Challenger     1 = Gagnant, 0 = Non

        if (iaWin == 1 || userWin == 1) {       //S'il y a un gagnant, imprimer sur la console et terminer le cycle
            outputWinner(userWin, iaWin); //Afficher un gagnant
            return 1;
        }
        return 0;
    }

    @Override
    public void printRulesOfGame(String[] rulesOfGame) {
        defender.printRulesOfGame(rulesOfGame);
    }

    @Override
    public void printTour(int gameTourNumber) {
        defender.printTour(gameTourNumber);
    }

    @Override
    public void outputWinner(int userWin, int iaWin) {
        challenger.outputWinner(userWin, iaWin);
    }

    /*
        S'il n'y a pas de gagnants et qu'il s'agit du dernier Tour du jeu, afficher les informations et lancer le menu de fin de jeu.
     */
    @Override
    public void checkStandoff(int gameTourNumber, String currentMod) {
        challenger.checkStandoff(gameTourNumber, currentMod);
    }
}
