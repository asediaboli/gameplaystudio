package mode;

import config.ConfigGame;
import menu.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.Util;

abstract class Mode implements ModeInterface {

    private static final Logger LOG = LogManager.getLogger(Mode.class.getName());

    static final int NUMBERS_IN_GAME_SET = ConfigGame.getInstance().getNumberOfNumbersInGameSet();    //le nombre de chiffres de la combinaison
    static final int NUMBER_OF_ATTEMPTS = ConfigGame.getInstance().getNumberOfAttempts();        //Nombre d'essais
    private static final int DEV_MOD = ConfigGame.getInstance().getModeDev();

    int[] secretCombinationOfAI;
    int gameTourNumber;
    String responsePlayer;
    //Menu menu;
    String currentMode;


    Mode() {
        this.currentMode = getClass().getSimpleName();
        //this.menu = new Menu(currentMode);
    }

    /*
    Vérification du mode développeur et affichage d'une combinaison secrète si nécessaire
     */
    static void checkDevMod(int[] answerIA, int gameTourNumber) {
        if (gameTourNumber == 1) {
            StringBuilder response = new StringBuilder("<DEV_MODE>Combinaison secrète: ");
            if (DEV_MOD == 1) {
                response.append(Util.arrayToString(answerIA));
                System.out.println(response.append(" <DEV_MODE>").toString());
                LOG.info(response.toString());
            }
        }
    }

    /*
    Méthode principale
 */
    void start(String[] rulesOfGame) {
        int winner;
        printRulesOfGame(rulesOfGame); //Appeler le menu des règles
        while (gameTourNumber <= NUMBER_OF_ATTEMPTS) {
            printTour(gameTourNumber); //Numéro du Tour de jeu
            winner = startTour(gameTourNumber);
            if (winner == 1) {
                outputWinner(winner);
                getMenu(currentMode);
                break;
            }
            checkStandoff(gameTourNumber, currentMode);
            gameTourNumber++;
        }
    }

    @Override
    public void printRulesOfGame(String[] rulesOfGame) {
        System.out.println(rulesOfGame[0]);
        System.out.println(rulesOfGame[1]);
        System.out.println(rulesOfGame[2]);
    }

    @Override
    public void printTour(int gameTourNumber) {
        String s = (gameTourNumber == NUMBER_OF_ATTEMPTS ? "\\\\***Dernier Tour " + gameTourNumber + "***//" : "\\\\***Tour " + gameTourNumber + "***// ");
        LOG.info(s);
        System.out.println(s);
    }

    /*
       Vérifier s'il y a un gagnant. Si c'est le cas, retirer le gagnant et commencer la fin du jeu.

       1 = Gagnant
       0 = Non
    */
    @Override
    public void outputWinner(int userWin, int iaWin) {
        if (userWin == 1 & iaWin == 1) {
            LOG.info("**Pas de gagnants**");
            System.out.println("**Egalité**");
        } else if (userWin == 1 & iaWin == 0) {
            LOG.info("**Joueur a gagné**");
            System.out.println("**Vous avez gagné**");
        } else if (iaWin == 1 & userWin == 0) {
            LOG.info("**Joueur a perdu**");
            System.out.println("**Vous avez perdu**");
        }

    }

    /*
         S'il n'y a pas de gagnants et qu'il s'agit du dernier Tour du jeu, afficher les informations et lancer le menu de fin de jeu.
      */
    @Override
    public void checkStandoff(int gameTourNumber, String currentMode) {
        if (gameTourNumber == NUMBER_OF_ATTEMPTS) {
            if (currentMode.equalsIgnoreCase("DEFENDER")) {
                LOG.info("**Joueur a gagné**");
                System.out.println("**Vous avez gagné**");
            } else if (currentMode.equalsIgnoreCase("CHALLENGER")) {
                LOG.info("**Joueur a perdu**");
                System.out.println("**Vous avez perdu**");
                System.out.println("Combinaison secrète " + Util.arrayToString(secretCombinationOfAI));
            } else if (currentMode.equalsIgnoreCase("DUEL")) {
                LOG.info("**Pas de gagnants** \n" +
                    "Combinaison secrète " + Util.arrayToString(secretCombinationOfAI));
                System.out.println("**Pas de gagnants** \n" +
                    "Combinaison secrète " + Util.arrayToString(secretCombinationOfAI));
            }
            getMenu(currentMode);
        }
    }

    void outputProposition(String info) {
        LOG.info(info);
        System.out.println(info);
    }

    private void getMenu(String currentMode) {
        if (currentMode == null) {
            Menu menu = new Menu();
            menu.startMenu();
        } else {
            Menu menu = new Menu(currentMode);
            menu.startMenu();
        }
    }

    public abstract int startTour(int gameTourNumber);

    abstract void outputWinner(int winner);
}
