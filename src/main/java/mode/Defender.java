package mode;

import io.IOClass;
import io.RandomNumbers;
import util.Util;

public class Defender extends Mode {

    private int[] numberSuggestedByAI;

    //Description du mode et des règles du jeu
    private final String[] rulesOfGame = {"****Bienvenue en mode Defender**** \n***********Règles du jeu***********",

        "1. Joueur (cette fois dans le rôle de défenseur) définit une combinaison de " + Mode.NUMBERS_IN_GAME_SET + " chiffres\n"
            + "2. Intelligence artificielle de l’ordinateur doit faire une proposition d’une combinaison de " + Mode.NUMBERS_IN_GAME_SET + " chiffres\n"
                    + "3. Joueur indique pour chaque chiffre de la combinaison proposée si le chiffre de sa combinaison est plus grand (+), plus petit (-) ou si c’est le bon (=)\n"
            + "4. Nombre d’essais sont " + Mode.NUMBER_OF_ATTEMPTS + "\n",

        "Imaginez " + Mode.NUMBERS_IN_GAME_SET + " chiffres et appuyez sur Enter"};

    /*
        Constructeurs
     */
    Defender() {
        this.gameTourNumber = 1;
    }


    @Override
    public void start() {
        super.start(rulesOfGame);
    }

    /*
            Méthode de base. Résultat = présence du vainqueur dans ce printTour de jeu
     */
    @Override
    public int startTour(int gameTourNumber) {
        numberSuggestedByAI = RandomNumbers.takeSuggestedByAI(gameTourNumber, numberSuggestedByAI, Mode.NUMBERS_IN_GAME_SET, responsePlayer); // Tableau de nombres proposé par l'intelligence artificielle
        outputInfoPropositionAI(numberSuggestedByAI);
        responsePlayer = IOClass.inputPlayerResponse(Mode.NUMBERS_IN_GAME_SET);   //Réponse du joueur (+-+-)
        if (RandomNumbers.isWin(responsePlayer, Mode.NUMBERS_IN_GAME_SET)) {
            return 1;
        } else {
            outputProposAndRespon(numberSuggestedByAI);
            return 0;
        }
    }

    /*
        Sortie sur la console des propositions de l’intelligence artificielle et de la réponse du joueur
     */
    private void outputProposAndRespon(int[] arrayToDisplay) {
        String s = "Proposition  d'IA: "
                + Util.arrayToString(arrayToDisplay)
                + " -> Votre réponse : "
                + this.responsePlayer;
        super.outputProposition(s);

    }

    /*
        Sortie d'information sur l'étape du jeu
     */
    private void outputInfoPropositionAI(int[] numberSuggestedByAI) {
        System.out.println("Proposition de l'IA : " + Util.arrayToString(numberSuggestedByAI) + "\n" +
                "Indiquez pour chaque chiffre de la combinaison proposée si le chiffre de votre combinaison est plus grand (+), plus petit (-) ou si c’est le bon (=).");
    }

    @Override
    void outputWinner(int iaWin) {
        super.outputWinner(0, iaWin);
    }

    @Override
    public void printRulesOfGame(String[] rulesOfGame) {
        super.printRulesOfGame(rulesOfGame);
        Util.pressEnterToContinue();
    }
}
