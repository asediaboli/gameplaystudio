package util;

import io.RandomNumbers;
import org.junit.Assert;
import org.junit.Test;

public class RandomNumbersTest {

    @Test
    public void randomSet() {
        int[] expexted = RandomNumbers.takeFollowingSetAI(4);
        for (int i : expexted){
            System.out.print(i + " ");
        }
    }

    @Test
    public void comparisonResult() {
        int[] randUser = {4,2,4,1};
        int[] randComp = {3,4,1,2};
        String expected = RandomNumbers.takeArrayComparisonResult(randUser, randComp);
        String actual = "+-+-";
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void responseArtInt() {
        int[] intSet = {5, 6, 3, 8};
        String temp = "-+-=";

        int[] expect = RandomNumbers.takeResponseAI(intSet, temp);

        int[] actual = {4, 7, 2, 8};
        Assert.assertArrayEquals(expect, actual);

    }
}