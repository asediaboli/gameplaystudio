package util;

import config.ConfigGame;
import org.junit.Test;

public class ConfigGameTest {

    @Test
    public void getInstance() {
        System.out.println("ModeDev = " + ConfigGame.getInstance().getModeDev());
        System.out.println("QuantityNumbers = " + ConfigGame.getInstance().getNumberOfNumbersInGameSet());
        System.out.println("NumberOfTests = " + ConfigGame.getInstance().getNumberOfAttempts());
    }
}